//
//  GetStudents.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 9/25/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

class GetStudents {
    

    static func getStudentFromJamfSchool(fromWhichGroupNumber groupNumber: String?, giveMeEachStudent completionHandler:  @escaping ([User]) -> Void )  {
       
        var groupNumberNotNil = ""
        
        if  groupNumber != nil { groupNumberNotNil = groupNumber! } else { groupNumberNotNil = UserDefaulltsHelper.readGroupUserDefaults() }
        
        GetDataApi.getUserListByGroupResponse (GeneratedReq.init(request: ValidReqs.usersInDeviceGroup(parameterDict: ["memberOf" : groupNumberNotNil]) )) { (usrResponse) in
            DispatchQueue.main.async {
                guard var usrResponse = usrResponse as? UserResponse else {fatalError("could not convert it to Users")}
                usrResponse.users.sort()
                completionHandler(usrResponse.users)
            }
        }
    }
}
